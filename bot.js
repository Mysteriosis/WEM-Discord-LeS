const fs = require('fs');
const Discord = require('discord.js');
var http = require('http').createServer();
var io = require('socket.io')(http);

const config = require('./config.json');
const action = process.argv[2];

// Text treatment
const stopwordsEN = require('stopwords-en');
const stopwordsFR = require('stopwords-fr');
const stopwords = stopwordsFR.concat(stopwordsEN);
const regexWordsFR = /(([^\W\d_]|[àâçéèêëîïôûùüÿñæœ]){3,})+/g;

// ID du serveur LeS
// const guildID = '126269621123678208';
const client = new Discord.Client();

// ID des channels
const listChannels = new Map([
  ['testFG', '285363958259646465'],
  ['gaCounterstrike', '126305067782373376'],
  ['gaLeagueoflegends', '176702463942852608'],
  ['gaHearthstone', '126305029673058304'],
  ['gaRocketleague', '273722552927518720'],
  ['gaOverwatch', '215087171055517697'],
  ['gaFifa', '293371128024399872'],
  ['randomTwitch', '248440939771723786'],
  ['randomTechnologie', '126305399996416000'],
  ['randomHardware', '180724031786319872'],
  ['randomFun', '126305441713094656'],
  ['randomNews', '182745965390856192'],
  ['publique', '180691908761223170']
]);

let dictionnary = new Map();
// let words = new Array();

class Msg {
  constructor(m) {
    this.id = m.id;
    this.author = {
      id:m.author.id,
      username:m.author.username
    };
    this.content = m.content;
    this.reactions = m.reactions.map(v => ({count: v.count, emoji: v.emoji.name}));
    this.mentions = {
      everyone: m.mentions.everyone,
      users: m.mentions.users.map(v => ({id: v.id, username: v.username}))
    };
    this.created_at = m.createdTimestamp;
  }

  toString() {
    return `
{ "id": ${this.id}, "author": { "id": ${this.author.id}, "username": "${this.author.username}" }, "content": "${this.content}", "reactions": "${this.reactions}", "mentions": { "everyone": ${this.mentions.everyone}, "users": ${this.mentions.users} }, "created_at": ${this.created_at} }`;
  }
}

function addToCollection(data) {
  console.time('Words extract');
  let newWords = [];
  for(let message of data) {
    if(message !== undefined) {
      let extract = message.content.match(regexWordsFR);
      if(extract)
        extract.filter(word => !stopwords.includes(word)).map(word => {
          const loWord = word.toLowerCase();
          if(dictionnary.has(loWord)){
            let num = dictionnary.get(loWord);
            dictionnary.set(loWord, num+1);
          }
          else {
            dictionnary.set(loWord, 1);
          }
        });
    }
  }
  newWords = ([...dictionnary]).filter(word => word[1] > 2).sort();
  console.timeEnd('Words extract');
  return newWords;
}

function populate(name, data) {
  // Write in file
  fs.writeFile('./data/' + name + '.json', JSON.stringify(data), (err) => {
    if (err) console.error(err);
    console.log('Process Done.');
    client.destroy();
    process.exit();
  });
}

function serve(name, data) {
  // HTTP
  http.listen(3000, function(){
    console.log('Loading server on *:3000');
  });

  // WEBSOCKET
  io.on('connection', function(socket) {
    console.log('User connected');
    io.emit('discord collection', { title: name, wordsList: addToCollection(data) });

    socket.on('disconnect', function() {
      console.log('User disconnected');
    });
  });

  client.on('message', message => {
    if(message.author !== client.user) return;

    // Create Msg Object & send the new words to client
    let msg = new Msg(message);
    io.emit('discord message', { title: name, wordsList: addToCollection(data.concat(msg)) });
    // rest of the code for commands go here
  });
}

function getMessages(channelName, nb, oldMsg) {
  const channel = client.channels.get(listChannels.get(channelName));
  const lastMessageID = (oldMsg !== undefined) ? [...oldMsg].pop().id : undefined;
  let messagesCollection = (oldMsg !== undefined) ? oldMsg : [];

  channel.fetchMessages({limit:100, before: lastMessageID})
    .then(
      msgs => {
        if(msgs.size === 0)
          nb = -1;
        else
          msgs.filter(m => m !== undefined).forEach(m => messagesCollection.push(new Msg(m)));

        if(nb>0)
          messagesCollection.push(getMessages(channelName, nb-1, messagesCollection));
        else {
          switch(action) {
          case 'serve':
            serve(channelName, messagesCollection);
            break;
          case 'populate':
          default:
            populate(channelName, messagesCollection);
          }
        }
        return messagesCollection;
      })
      .catch(err => {
        console.error(err);
        client.destroy();
        process.exit();
      });
}

// Start Discord requests
client.login(config.token);
client.on('ready', () => {
  console.log('Connected to Discord.');
  console.log('Get messages from "' + config.channel + '" ...');
  getMessages(config.channel, config.getMessagesNbTimes);
});
