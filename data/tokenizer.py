# -*- coding: utf-8 -*-

import string
from nltk.corpus import stopwords
from nltk.tokenize import wordpunct_tokenize
from nltk.stem.snowball import FrenchStemmer

import sys
#reload(sys)
#sys.setdefaultencoding('utf-8')

stop = stopwords.words("french") + stopwords.words("english") + list(string.punctuation)
stemmer = FrenchStemmer()

def tokenize(str):
    return [stemmer.stem(word) for word in wordpunct_tokenize(str.lower()) if word not in stop]

