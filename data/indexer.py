import sys
import json
import re
import string
import tokenizer
import math
import tfidf_tools


def computeTFIDF(index, nbChannels, channelFreqSum):
    # Iterate over token
    for token, channels in index.items():
        df = len(channels) # Compute document frequency how many channels mention this token
        idf = tfidf_tools.idf(df, nbChannels)

        # Iterate over channels
        for channel, freq in channels.items():
            tf = float(freq) / channelFreqSum[channel] # Normalize tf
            index[token][channel] = tf * idf

def main(argv):
    tokens = {} # inverted index
    channelFreqSum = {} # sums of tf by channel

    # Iterate over channel
    for f in argv:
        with open(f) as data_file:
            data = json.load(data_file)

        channel = f[:-5] # name without .json
        channelFreqSum[channel] = 0

        for msg in data: # Iterate over messages in channel
            if not isinstance(msg, dict): # some parse error
                continue

            # Extract the message, delete all < > balise (mentions, images, ...)
            message = re.sub("<.*?>", "", msg["content"])

            # Iterate over tokens of the message content
            for token in tokenizer.tokenize(message):
                channelFreqSum[channel] += 1
                if token in tokens: # The token exists
                    if channel in tokens[token]: # The token exists in the current channel
                        tokens[token][channel] += 1
                    else: # New token for this channel
                        tokens[token][channel] = 1
                else: # New token
                    tokens[token] = {}
                    tokens[token][channel] = 1

    computeTFIDF(tokens, len(argv), channelFreqSum)

    # Save the index on the disk
    with open('index.json', 'w') as fp:
        json.dump(tokens, fp)

if __name__ == "__main__":
    main(sys.argv[1:])