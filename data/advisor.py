import sys
from sys import stdin
import json
import tokenizer
import math
import tfidf_tools


def main(argv):

    # Load the index
    file = "index.json"
    with open(file) as data_file:
        index = json.load(data_file)

    # Get user message
    print "Enter your message: "
    userinput = unicode(stdin.readline(), "utf-8")

    # Tokenize the message
    tokens = tokenizer.tokenize(userinput)

    qFreqSum = 0 # Sum of tf for the query
    qTf = {} # tf for the query { <token> : <tf> }
    # Iterate over user's message tokens, compute tf
    for token in tokens:
        qFreqSum += 1
        if token in qTf:
            qTf[token] += 1
        else:
            qTf[token] = 1

    qTfIdf = {} # Query tfidf { <token> : <tfidf> }
    chTfIdf = {} # Channel tfidf { <channel> : { <token> : <tfidf> } }
    for channel in argv: # Initialize inner dictionnaries
        chTfIdf[channel] = {}

    # Iterate over query tf
    for token, tf in qTf.items():
        if token in index: # Consider only tokens indexed
            qDf = len(index[token]) # df of token
            # Compute query tfidf, first normalize query tf
            qTfIdf[token] = float(tf) / qFreqSum * tfidf_tools.idf(qDf, len(argv))

            # Fetch all channel tfidf based on the token
            for channel in argv:
                if channel in index[token]:
                    chTfIdf[channel][token] = index[token][channel]
                else:
                    chTfIdf[channel][token] = 0.0

    similarties = {}
    qNorm = tfidf_tools.cosineNormalisation(qTfIdf.values()) # norm value for query vector
    for channel in argv: # Compute cosine similarity between query and all channels
        dotProduct = 0.0
        for token, tfidf_norm in qTfIdf.items(): # Compute dot product
            dotProduct += tfidf_norm * chTfIdf[channel][token]
        if dotProduct == 0: # The similarity will be zero, don't consider this channel
            continue
        chNorm = tfidf_tools.cosineNormalisation(chTfIdf[channel].values()) # norm value for channel vector
        similarties[channel] = dotProduct / (qNorm * chNorm) # normalize to obtain the cos similarity


    print "Recommanded channels:"
    for val in sorted(similarties.items(), key=lambda x: x[1], reverse=True): # List considered channels in decreasing order
        print  "%s similarity : %f" % (val[0], val[1])


if __name__ == "__main__":
    main(sys.argv[1:])





