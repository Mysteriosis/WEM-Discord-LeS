const fontMin = 10, fontMax = 100;
const maxCallback = ( acc, cur ) => Math.max( acc, cur[1] );
let wordsMax = 1;
let socket = io('http://localhost:3000');
let wordsList = [];
let layout = {};

function wordCloud() {
  layout = d3.layout.cloud()
    .size([800, 600])
    .words(wordsList.map(function(d) {
      return {text: d[0], size: (d[1] / wordsMax) * (fontMax - fontMin) + fontMin};
    }))
    .padding(8)
    .rotate(() => (~~(Math.random() * 6) - 3) * 30)
    .font('Roboto')
    .fontSize(function(d) { return d.size; })
    .on('end', draw);

  layout.start();

  function draw(words) {
    d3.select('div').append('svg')
      .attr('width', layout.size()[0])
      .attr('height', layout.size()[1])
    .append('g')
      .attr('transform', 'translate(' + layout.size()[0] / 2 + ',' + layout.size()[1] / 2 + ')')
    .selectAll('text')
      .data(words)
    .enter().append('text')
      .style('font-size', function(d) { return d.size + 'px'; })
      .style('font-family', 'Roboto')
      .style('fill', function(d, i) { return d3.schemeCategory20[i]; })
      .attr('text-anchor', 'middle')
      .attr('transform', function(d) {
        return 'translate(' + [d.x, d.y] + ')rotate(' + d.rotate + ')';
      })
      .text(function(d) { return d.text; });
  }
}

$(document).ready(function() {

  socket.on('discord collection', function(data){
    console.log('Collection', data);
    $('#messages').empty();
    wordsList = data.wordsList;
    wordsMax = wordsList.reduce(maxCallback, -Infinity);
    $('#title').text('Word Cloud for ').append($('<em>').text('"' + data.title + '"'));
    wordCloud();
  });

  socket.on('discord message', function(data){
    // wordsList = wordsList.concat(data);
    // $('#messages').empty();
    // wordCloud();
    // console.log('Message', data);
    // console.log('WordCloud', wordsList);
    console.log('Collection', data);
    $('#messages').empty();
    wordsList = data.wordsList;
    wordsMax = wordsList.reduce(maxCallback, -Infinity);
    $('#title').text('Word Cloud for ').append($('<em>').text('"' + data.title + '"'));
    wordCloud();
  });

});
