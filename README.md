# WEM-Discord-LeS
Computing messages from a large Discord server

## Usage
- Clone repository.
- Rename a `config.json.example` to `config.json`.
- Replace `token` value by your OAuth2 Discord token in this file (***you have to be invited on Lausanne eSport Discord's server, or use an other one.***).
- Replace `channel` with the channel name (list below) that you want to extract:

```
- 'testFG' (test from FestiGeek Server)
- 'gaCounterstrike'
- 'gaLeagueoflegends'
- 'gaHearthstone'
- 'gaRocketleague'
- 'gaOverwatch'
- 'gaFifa'
- 'randomTwitch'
- 'randomTechnologie'
- 'randomHardware'
- 'randomFun'
- 'randomNews'
- 'publique'
```

- Run `npm install` to install dependencies
- And finally :
  - `npm run server` to start an online WordCloud server. (Open `html/index.html` file to see the result **Node server doesn't serve the page**)
  - `npm run populate` to create a json file with Discord messages.

# Rapport
## Contexte et objectifs du projet
[Discord](https://discordapp.com/) est un logiciel qui a révolutionné la VoIP dans le monde des jeux en lignes. Offrant une plateforme web hautement accessible gratuitement pour tous, il a su s'imposer là où [Skype](https://www.skype.com/fr/) et [TeamSpeak](https://www.teamspeak.com/) semblaient être les seuls à se battre.
Cependant, en offrant par la suite des possibilités de chats textuels tout aussi évoluées que [Slack](https://slack.com/), Discord à fini par convaincre bien au delà de son publique d'origine à savoir les joueurs.

Le but de ce projet est de tenter, grâce à l'[API Discord](https://discordapp.com/developers/docs/intro) fournie, d'effectuer des analyses sur le contenu d'un serveur en particulier afin de pouvoir, entre autre, afficher un WordCloud sur les mots clefs les plus utilisés dans un salon, ou encore permettre d'effectuer une recherche afin de déterminer les salons les plus en adéquation avec notre recherche.

## Données (sources, quantité, évtl. pré-traitement, description)
### Source
Serveur Discord du [Lausanne e-Sport](https://www.lausanne-esports.ch/).

### Quantité
**2100 messages** environ par salon, moins si ces derniers n'en n'ont pas suffisamment.

### Pré-traitement
Lors de l'extraction, un premier filtrage par **stop word** [français](https://github.com/stopwords-iso/stopwords-fr) **ET** [anglais](https://github.com/stopwords-iso/stopwords-en) est fait sur le contenu du message (l'univers du jeu comprenant constamment des anglicismes).
On **retire** également plusieurs champs inutiles d'après notre cahier des charges :
- **`channel_id` :** déjà connu
- **`edited_timestamp` :** seul compte sa date de création
- **`tts` :** (booléen) si le message a été utilisé pour de la synthèse vocale (fonctionnalité Discord)
- **`mention_roles` :** mention de groupes propres au serveur, inutilisable sans gestions des groupes existants
- **`attachments` :** fichiers joints, hors sujet
- **`embeds` :** notifications externes aux salons (généralement écrit par des bots)

### Description
#### Format des messages Discord
```json
{
    "id": "162701077035089920",
    "channel_id": "131391742183342080",
    "author": {},
    "content": "Hey guys!",
    "timestamp": "2016-03-24T23:15:59.605000+00:00",
    "edited_timestamp": null,
    "tts": false,
    "mention_everyone": false,
    "mentions": [],
    "mention_roles": [],
    "attachments": [],
    "embeds": [],
    "reactions": []
}
```

#### Format de stockage du projet
```json
{
  "id":"162701077035089920",
  "author":{
    "id":"",
    "username":""
    },
  "content":"Hey guys!",
  "reactions":[],
  "mentions": {
    "everyone":false,
    "users":[]
  },
  "created_at":1495815313647
}
```

## Planification, répartition du travail
- **Récupération des données sur Discord :** P-Alain Curty
- **Traitement des données :** P-Alain Curty & Jérôme Moret
- **WordCloud :** P-Alain Curty
- **Recherche & Index inversé :** Jérôme Moret

## Fonctionnalités / cas d’utilisation

### WordCloud

La commande `npm run server` permet de lancer un micro-serveur web proposant une page générant un WordCloud. Il s'agit d'une mosaïque de mots ou ces derniers sont de plus en plus gros en fonction de leur nombre d'occurences dans un salon donné (configurable dans `config.json`).

Ce dernier reste également actif afin de continuer à capter les ajouts dans le salon côté Discord, afin de mettre à jour le WordCloud lors de la publication d'un nouveau message.

Afin de généré le WordCloud, une `Map` est crée en JavaScript contenant le mot extrait et son nombre d'occurence (insensible à la casse) :

```js
const regexWordsFR = /([^\W\d_]{2,}|[àâçéèêëîïôûùüÿñæœ])+/g;
/*...*/
let extract = message.content.match(regexWordsFR);
if(extract) {
  extract.filter(word => !stopwords.includes(word)).map(word => {
    const loWord = word.toLowerCase();
    if(dictionnary.has(loWord)){
      let num = dictionnary.get(loWord);
      dictionnary.set(loWord, num+1);
    }
    else {
      dictionnary.set(loWord, 1);
    }
  });
}
```

### Recommandation de channel
On cherche ici à proposer, selon un message entré par l'utilisateur, le channel ayant le plus de correspondance. Dans un monde idéal, un bot *Discord* proposerait cette recommandation à la volée lors de l'écriture du message.

## Techniques, algorithmes et outils utilisés

### Recommandation de channel

Nous avons créé un **index inversé** contenant pour chaque token une liste de channel pondérée selon **TF-IDF**.

Le travail a été réalisé à l'aide de scripts Python. Un premier script `indexer.py` s'occupe de créer l'index inversé selon la liste de channels (fichier `.json`) passée en argument. Un fichier `index.json` est généré. Le script `advisor.py` permet de tester la recommandation selon l'index (fichier `.json`) passé en paramètre. Il demande à l'utilisateur de saisir son message puis affiche après confirmation les channels recommandés.

#### Indexeur  

La récupération des tokens est effectuée en parcourant les messages contenus dans le fichier **JSON** relatif à chaque channel. Les messages contenant des citations, mentions incluent dans leur contenu une balise `<...>`. Un moyen simple de filtrer ses balises est l'utilisation des expressions régulières.

```python
re.sub("<.*?>", "", msg["content"])
```

Un filtrage est réalisé sur deux listes de **stopwords**, une pour la langue française, une pour l'anglais. Un **racinisateur** pour la langue française est utilisé afin d'optimiser la tokénisation.

#### Recommandeur

La recommandation est effectuée en récupérant les **tokens** relatifs au message. On construit alors un vecteur pour la requête ayant les composantes **TF-IDF** des **tokens**. On créé ensuite un vecteur par channel pour les mêmes tokens. Une **similarité par cosinus** est réalisée entre les paires (requête, channel) pour déterminer les plus pertinents.

## Conclusion

On relèvera l'excellente qualité de l'API qui permet assez aisément de récupérer des données et ce dans un format facilement traitable.  
Bien que notre résultat final paraisse peu évolué, il démontre tous de même la possibilité de transposition de ce genre d'outils en "bot", afin que les résultats soient disponibles en live dans un serveur Discord, mais également des occasions qui s'offrent à nous si Discord continue d'évoluer pour devenir un outil incontournable en gestion de projet par exemple.

En effet, comme cité plus haut, ses fonctionnalités sont très proches de Slack, il ne lui manque que quelques bot vers des services tiers pour devenir une alternative sérieuse à ce dernier.  
Dans ce genre de cas, il serait intéressant d'effectuer un traitement pour sonder, par exemple, les tensions dans un groupe de travail ou encore déterminer les membres les moins productifs, pour autant que les fonctionnalités de Discord soient correctement utilisées dans leur ensemble (citations, réaction, etc.).
